# ChainDB

Um componente para persistência de dados em formato relacional com algumas características adicionais:

- [X] Salva histórico para cada registro de cada tabela em um banco de um conjunto de nós
- [   ] Sincronização de dados em um cluster com múltiplos pontos (é feita através de mecanismo externo, como [Dropbox](https://www.dropbox.com) ou [Syncthing](https://syncthing.net/))
- [   ] Sincronização livre de conflitos entre pontos, quando a mesma linha é alterada em pontos diferentes duas versões da linha passam a coexistir no banco, sendo possível saber quais linhas possuem concorrência


## Conceitos centrais:

Um **instância** contém um ou mais **nós** sincronizados entre sí. Um **nó** possui um ou mais **bancos**. Um **banco** possui uma ou mais **entidades**. Uma **entidade** possui nenhum ou mais **registros**. Um **registro** possui uma ou mais **ocorrencias** relacionadas entre si.

### Onde:

- **instância (instance):** Conjunto de um ou mais pontos.

- **ponto (node):** Um ou mais sistemas computacionais que sincronizam arquivos entre sí através de qualquer mecanismo externo (como [Dropbox](https://www.dropbox.com) ou [Syncthing](https://syncthing.net/)). Este componente apenas provê as informações para manter os dados de dois ou mais nós sincronizados, mas não provê qualquer mecanismo para realizar as transferências necessárias de arquivos.

- **banco (database):** Conjunto de entidades relacionadas entre si que residem no mesmo nó e que são sincronizadas conjuntamente.

- **entidade (entity):** Conjunto de registros com características (colunas) em comum. Corresponde ao conceito de tabela em um banco de dados relacional.

- **registro (record):** Conjunto de ocorrências relacionadas entre sí que determina uma cadeia de alterações de uma informação. Semelhante ao conceito de linha de um banco de dados relacional, porém, além disso inclui um histórico de alterações com possíveis conflitos, resoluções, etc

- **ocorrência (occurrence):** Um ponto no histórico de uma linha. Pode possuir nenhuma, uma ou mais ocorrências predecessoras e/ou antecessoras. No momento é implementado como uma linha em uma tabela em um banco de dados [SQLite](https://sqlite.org/)

- **ocorrência base (base occurrence):** Primeira ocorrência de um registro. Não possui ocorrências antecessoras.

- **ocorrência topo (top occurrence):** Ocorrência mais recente de um registro. Não possui ocorrências sucessoras.

- **ocorrências concorrentes (concurrent occurrencies):** Conjunto de duas ou mais ocorrências com possuem um ocorrência antecessora em comum.

- **ocorrência de junção (merge occurrence):** Ocorrência com mais de um sucessor. Conceitualmente semelhando a um commit de merge do git.
