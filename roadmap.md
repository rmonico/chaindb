# Roadmap MVP

- ~~Refatorar testes já existentes~~
  - ~~Mudar o nome do UserTableMatcher para UserTableModelMatcher (depois vai ter o UserTableMatcher, para verificar a tabela do banco de dados)~~
  - ~~Rodar o teste na nova versão para ver se funciona~~
  - ~~Continuar fazendo os métodos describe do atual UserTableMatcher~~
- Fazer testes de salvar dados no banco a partir do modelo e verificar a estrutura criada (br.zero.chaindb.ChainDBTests.~~should_save_a_single_line_with_no_history~~ e should_save_a_line_with_history)
- Ver se os pacotes de produção têm alguma dependência dos pacotes de teste
- Criação de nova versão para a linha:
     - Fazer método para criar nova versão da linha
     - Fazer método na linha para marcá-la como excluída
     - Fazer método para criar linha de solução de conflito (mesmo método de cópia deve suportar receber mais uma linha inicial)
     - Todos os anteriores: antever e testar situações onde há relacionamento entre objetos persistentes (atualização das Foreign Key no DB)
- Testes de sincronização a partir de arquivos gerados
- Testes de geração de arquivos de sincronização
- Estudar extrair uma classe de DAO genérica
- Listar melhor situações de concorrência
     - Concorrência simples: duas versões da linha jcom o mesmo pai
     - Concorrência com ancestral a mais de um grau de profundidade
     - Escrever testes
