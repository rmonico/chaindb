package br.zero.chaindb;

import java.sql.Connection;
import java.sql.SQLException;

import br.zero.chaindb.dao.NodeDao;
import br.zero.chaindb.model.Node;

public interface Environment {

    void makeConnection() throws SQLException;

    void closeConnection() throws SQLException;

    Connection getConnection();

    IDGenerator getIdGenerator();

    void makeDAOs() throws SQLException;

    NodeDao getNodeDAO();

    StatementFactory getStatementFactory();

	Node getThisNode();
}
