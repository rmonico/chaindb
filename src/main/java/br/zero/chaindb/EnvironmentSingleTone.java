package br.zero.chaindb;

import java.sql.Connection;
import java.sql.SQLException;

import br.zero.chaindb.dao.NodeDao;
import br.zero.chaindb.model.Node;

public enum EnvironmentSingleTone implements Environment {

    ENVIRONMENT;

    private Environment instance;

    public void injectEnvironment(Environment instance) {
        this.instance = instance;
    }

    public Connection getConnection() {
        return instance.getConnection();
    }

    public NodeDao getNodeDAO() {
        return instance.getNodeDAO();
    }

    public IDGenerator getIdGenerator() {
        return instance.getIdGenerator();
    }

    @Override
    public void makeConnection() throws SQLException {
        instance.makeConnection();
    }

    @Override
    public void closeConnection() throws SQLException {
        instance.closeConnection();
    }

    @Override
    public void makeDAOs() throws SQLException {
        instance.makeDAOs();
    }

    @Override
    public StatementFactory getStatementFactory() {
        return instance.getStatementFactory();
    }

    @Override
    public Node getThisNode() {
        return instance.getThisNode();
    }

}