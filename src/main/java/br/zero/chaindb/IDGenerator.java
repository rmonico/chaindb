package br.zero.chaindb;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.zero.chaindb.model.Row;

public class IDGenerator {

    private String byteArrayToHexString(final byte[] b) {
        final StringBuilder result = new StringBuilder();

        for (int i = 0; i < b.length; i++)
            result.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));

        return result.toString();
    }

    public CharSequence makeId(final Row<?> row) throws IDGeneratorException {
        try (final ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            final ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(row);
            out.flush();

            final byte[] byteArray = bos.toByteArray();

            final MessageDigest digest = MessageDigest.getInstance("SHA1");

            return byteArrayToHexString(digest.digest(byteArray));
        } catch (final IOException | NoSuchAlgorithmException e) {
            throw new IDGeneratorException(e);
        }
    }

}
