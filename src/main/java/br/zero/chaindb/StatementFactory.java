package br.zero.chaindb;

import static br.zero.chaindb.EnvironmentSingleTone.ENVIRONMENT;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
public class StatementFactory {

    final private String pathFormat;
    final private Map<String, PreparedStatement> statementCache;

    public StatementFactory(final String pathFormat) {
        this.pathFormat = pathFormat;
        statementCache = new HashMap<>();
    }

    public String getString(String fileName) {
        String path = String.format(pathFormat, fileName);

        try {
            return loadUTF8TextFromFile(path);
        } catch (IOException e) {
            throw new RuntimeException("IOException should not happen here! Check your SQL resource files!", e);
        }
    }

    public PreparedStatement get(String fileName) throws SQLException {
        String statementPath = String.format(pathFormat, fileName);

        PreparedStatement statement = statementCache.get(statementPath);
        if (statement == null) {
            try {
                statement = createStatement(statementPath);
            } catch (IOException e) {
                throw new RuntimeException("IOException should not happen here! Check your SQL resource files!", e);
            }

            statementCache.put(statementPath, statement);
        }

        return statement;
    }

    private PreparedStatement createStatement(String filePath) throws IOException, SQLException {
        String sql = loadUTF8TextFromFile(filePath);

        PreparedStatement preparedStatement = ENVIRONMENT.getConnection().prepareStatement(sql);

        return preparedStatement;
    }

    private String loadUTF8TextFromFile(String filePath) throws IOException {
        final byte[] encoded = Files.readAllBytes(Paths.get(filePath));

        String sql = new String(encoded, "UTF-8");

        return sql;
    }

    public void invalidate() throws SQLException {
        for (Entry<String, PreparedStatement> entry : statementCache.entrySet()) {
            PreparedStatement statement = entry.getValue();

            statement.close();
        }
    }

}
