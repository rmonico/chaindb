package br.zero.chaindb.dao;

import static br.zero.chaindb.EnvironmentSingleTone.ENVIRONMENT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import br.zero.chaindb.model.Node;
public class NodeDao {

    private static final String GET_THIS_NODE_BY_ID = "select id, name, creation from __node__ where id = ?;";
    private PreparedStatement getNodeByNameStatement;
    private String thisNodeID;

    public NodeDao(final CharSequence thisNodeID) throws SQLException {
        this.thisNodeID = thisNodeID.toString();
        getNodeByNameStatement = ENVIRONMENT.getConnection().prepareStatement(GET_THIS_NODE_BY_ID);
    }

    public Node getThisNode() throws SQLException {
        getNodeByNameStatement.setString(1, thisNodeID);

        ResultSet resultSet = getNodeByNameStatement.executeQuery();

        if (!resultSet.next()) {
            return null;
        }

        Node node = new Node();

        node.setID(resultSet.getString("id"));
        node.setName(resultSet.getString("name"));
        Calendar creation = new GregorianCalendar();
        creation.setTime(resultSet.getTimestamp("creation"));
        node.setCreation(creation);

        return node;
    }

}
