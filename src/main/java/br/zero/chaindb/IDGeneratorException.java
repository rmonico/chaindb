package br.zero.chaindb;

public class IDGeneratorException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3088229952789158552L;

    public IDGeneratorException(final Exception e) {
        super(e);
    }

}
