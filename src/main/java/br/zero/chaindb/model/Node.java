package br.zero.chaindb.model;

import java.io.Serializable;
import java.util.Calendar;

public class Node implements Serializable {

    private static final long serialVersionUID = -4007507423364706819L;

    private CharSequence id;
    private CharSequence name;
    private Calendar creation;

    public CharSequence getID() {
        return id;
    }

    public void setID(final CharSequence id) {
        this.id = id;
    }

    public CharSequence getName() {
        return name;
    }

    public void setName(final CharSequence name) {
        this.name = name;
    }

    public Calendar getCreation() {
        return creation;
    }

    public void setCreation(final Calendar creation) {
        this.creation = creation;
    }

}
