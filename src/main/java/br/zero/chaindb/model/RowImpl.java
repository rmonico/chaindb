package br.zero.chaindb.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class RowImpl<T extends RowImpl<T>> implements Row<T>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5581333231311152677L;

    private transient CharSequence id;
    private Node node;
    private List<T> sucessors;
    private List<T> predecessors;

    public RowImpl() {
        sucessors = new LinkedList<>();
        predecessors = new LinkedList<>();
    }

    @Override
    public CharSequence getID() {
        return id;
    }

    public void setID(CharSequence id) {
        this.id = id;
    }

    @Override
    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Override
    public List<T> getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(List<T> predecessors) {
        this.predecessors = predecessors;
    }

    @Override
    public List<T> getSucessors() {
        return sucessors;
    }

    public void setSucessors(List<T> sucessors) {
        this.sucessors = sucessors;
    }

}
