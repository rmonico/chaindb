package br.zero.chaindb.model;

import java.util.List;

/**
 * Every implementation of Row must be a generic of itself!
 * 
 * @author rmonico
 *
 * @param <T>
 */
public interface Row<T extends Row<T>> {

    CharSequence getID();

    public Node getNode();

    public List<T> getPredecessors();

    public List<T> getSucessors();

}
