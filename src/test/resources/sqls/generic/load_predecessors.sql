select
  u.*

from
  __predecessor__ p

  left join usertable u on
    (u.id = p.predecessor)

where
  p.id = ?;
