select
  usertable.id,
  usertable.finaldata

from
  usertable

  left outer join __sucessor__ sucessor on
    (sucessor.id = usertable.id)

where
  sucessor.id is null;
