select
  u.finaldata as usertable_finaldata,

  o.id as object_id,
  o.creation as object_creation,

  n.id as node_id,
  n.creation as node_creation,
  n.name as node_name

from
  usertable u

  left join __object__ o on
    (u.id = o.id)

  left join __node__ n on
    (o.source_node = n.id);
