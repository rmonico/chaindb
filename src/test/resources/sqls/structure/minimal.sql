create table __node__
    (id text primary key,
    name text not null,

    creation datetime not null default (strftime('%Y-%m-%d %H:%M:%f', 'now')))
without rowid;

create table __sucessor__
    (id text primary key,
    sucessor text not null,

    foreign key(id) references __object__,
    foreign key(sucessor) references __object__)
without rowid;

create table __predecessor__
    (id text primary key,
    predecessor text not null,

    foreign key(id) references __object__,
    foreign key(predecessor) references __object__)
without rowid;

create table __object__
    (id text primary key,
    creation datetime not null default current_timestamp,
    source_node text not null,

    foreign key(source_node) references __node__)
without rowid;

create table usertable
    (id text primary key,
    finaldata text not null,

    foreign key(id) references __object__)
without rowid;

insert into __node__ (id, name) values ('23c2679cbf774ee18f6e048bdf60f7e1', 'Moningrado');

