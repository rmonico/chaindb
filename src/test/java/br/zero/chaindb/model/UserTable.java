package br.zero.chaindb.model;

import br.zero.chaindb.model.RowImpl;

public class UserTable extends RowImpl<UserTable> {

    private static final long serialVersionUID = -3067744859694341494L;

    private String data;

    public String getUserData() {
        return data;
    }

    public void setUserData(String data) {
        this.data = data;
    }

}
