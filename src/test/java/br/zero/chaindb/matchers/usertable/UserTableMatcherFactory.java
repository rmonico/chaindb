package br.zero.chaindb.matchers.usertable;

import org.hamcrest.Matcher;

import br.zero.chaindb.model.UserTable;

public class UserTableMatcherFactory {

    private Matcher<?> userDataMatcher;
    private Matcher<?> predecessorsMatcher;
    private Matcher<?> sucessorsMatcher;

    public static UserTableMatcherFactory whereUserTable() {
        return new UserTableMatcherFactory();
    }

    public UserTableMatcherFactory userData(final Matcher<?> userDataMatcher) {
        this.userDataMatcher = userDataMatcher;
        return this;
    }

    public UserTableMatcherFactory predecessors(final Matcher<?> predecessorsMatcher) {
        this.predecessorsMatcher = predecessorsMatcher;
        return this;
    }

    public UserTableMatcherFactory sucessors(final Matcher<?> sucessorsMatcher) {
        this.sucessorsMatcher = sucessorsMatcher;
        return this;
    }

    public Matcher<UserTable> ok() {
        return new UserTableMatcher(userDataMatcher, predecessorsMatcher, sucessorsMatcher);
    }
}
