package br.zero.chaindb.matchers.usertable;

import org.hamcrest.BaseMatcher;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import br.zero.chaindb.model.UserTable;

public class UserTableMatcher extends BaseMatcher<UserTable> {

    final private Matcher<?> userDataMatcher;
    final private Matcher<?> predecessorsMatcher;
    final private Matcher<?> sucessorsMatcher;

    public UserTableMatcher(final Matcher<?> userDataMatcher, final Matcher<?> predecessorsMatcher, final Matcher<?> sucessorsMatcher) {
        this.userDataMatcher = userDataMatcher != null ? userDataMatcher : CoreMatchers.anything();
        this.predecessorsMatcher = predecessorsMatcher != null ? predecessorsMatcher : CoreMatchers.anything();
        this.sucessorsMatcher = sucessorsMatcher != null ? sucessorsMatcher : CoreMatchers.anything();
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("A UserTableMatcher where userData ");
        description.appendDescriptionOf(userDataMatcher);

        description.appendText(", and predecessors being ");
        description.appendDescriptionOf(predecessorsMatcher);

        description.appendText(", and sucessors being ");
        description.appendDescriptionOf(sucessorsMatcher);
    }

    private static enum MatchResult {
        IS_NULL, IS_NOT_INSTANCEOF_USERTABLE, USERDATA_DOESNT_MATCH, PREDECESSORS_DOESNT_MATCH, SUCESSORS_DOESNT_MATCH, OK;

        public boolean isOk() {
            return this == OK;
        }
    }

    private MatchResult doMatch(final Object item) {
        if (item == null)
            return MatchResult.IS_NULL;

        if (!(item instanceof UserTable))
            return MatchResult.IS_NOT_INSTANCEOF_USERTABLE;

        final UserTable actual = (UserTable) item;

        if (!userDataMatcher.matches(actual.getUserData()))
            return MatchResult.USERDATA_DOESNT_MATCH;

        if (!predecessorsMatcher.matches(actual.getPredecessors()))
            return MatchResult.PREDECESSORS_DOESNT_MATCH;

        if (!sucessorsMatcher.matches(actual.getSucessors()))
            return MatchResult.SUCESSORS_DOESNT_MATCH;

        return MatchResult.OK;
    }

    @Override
    public boolean matches(final Object item) {
        return doMatch(item).isOk();
    }

    @Override
    public void describeMismatch(final Object objReceived, final Description mismatchDescription) {
        MatchResult matchResult = doMatch(objReceived);

        switch (matchResult) {
        case IS_NULL: {
            mismatchDescription.appendText("Received object was null.");
            break;
        }
        case IS_NOT_INSTANCEOF_USERTABLE: {
            mismatchDescription.appendText("Received object wasn't a instance of UserTable.");
            break;
        }

        case USERDATA_DOESNT_MATCH: {
            final UserTable received = (UserTable) objReceived;

            mismatchDescription.appendText("userData in received object was ");
            mismatchDescription.appendValue(received.getUserData());
            mismatchDescription.appendText(" but should be ");
            mismatchDescription.appendDescriptionOf(userDataMatcher);
            break;
        }

        case PREDECESSORS_DOESNT_MATCH: {
            final UserTable actual = (UserTable) objReceived;

            mismatchDescription.appendText("predecessors in received object were ");
            mismatchDescription.appendValue(actual.getPredecessors());
            mismatchDescription.appendText(" but should be ");
            mismatchDescription.appendDescriptionOf(predecessorsMatcher);
            break;
        }

        case SUCESSORS_DOESNT_MATCH: {
            final UserTable actual = (UserTable) objReceived;

            mismatchDescription.appendText("predecessors in received object were ");
            mismatchDescription.appendValue(actual.getSucessors());
            mismatchDescription.appendText(" but should be ");
            mismatchDescription.appendDescriptionOf(sucessorsMatcher);
            break;
        }

        default:
            mismatchDescription.appendText("BUG in UserTableModelMatcher: a MatchResult was not handled!");
            break;
        }
    }

}
