package br.zero.chaindb.matchers.usertableresultset;

import java.util.Collection;

import org.hamcrest.Matcher;

import br.zero.chaindb.matchers.usertable.UserTableMatcher;

public class UserTableResultSetRowMatcherFactory {

    private UserTableResultSetMatcherFactory parentFactory;
    private int rowNumber;
    private Matcher<String> dataMatcher;
    private Matcher<Collection<? extends Object>> predecessorsMatcher;
    private Matcher<Collection<? extends Object>> sucessorsMatcher;

    public UserTableResultSetRowMatcherFactory(UserTableResultSetMatcherFactory parentFactory, int rowNumber) {
        this.parentFactory = parentFactory;
        this.rowNumber = rowNumber;
    }

    public UserTableResultSetRowMatcherFactory data(Matcher<String> dataMatcher) {
        this.dataMatcher = dataMatcher;

        return this;
    }

    public UserTableResultSetRowMatcherFactory predecessorList(Matcher<Collection<? extends Object>> predecessorsMatcher) {
        this.predecessorsMatcher = predecessorsMatcher;
        return this;
    }
    
    public UserTableResultSetRowMatcherFactory sucessorList(Matcher<Collection<? extends Object>> sucessorsMatcher) {
        this.sucessorsMatcher = sucessorsMatcher;

        return this;
    }

    private UserTableMatcher createMatcher() {
        return new UserTableMatcher(dataMatcher, predecessorsMatcher, sucessorsMatcher);
    }
    
    public UserTableResultSetMatcherFactory endRow() {
        parentFactory.addRow(rowNumber, this.createMatcher());
        
        return parentFactory;
    }

}
