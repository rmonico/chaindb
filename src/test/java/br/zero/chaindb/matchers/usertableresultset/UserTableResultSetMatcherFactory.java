package br.zero.chaindb.matchers.usertableresultset;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;

import br.zero.chaindb.matchers.usertable.UserTableMatcher;

public class UserTableResultSetMatcherFactory {

    private Matcher<Integer> rowCountMatcher;
    private List<UserTableMatcher> rowMatchers;

    public static UserTableResultSetMatcherFactory whereUserTableResultSet() {
        return new UserTableResultSetMatcherFactory();
    }
    
    public UserTableResultSetMatcherFactory() {
        this.rowMatchers = new ArrayList<>();
    }

    public UserTableResultSetMatcherFactory rowCount(Matcher<Integer> rowCountMatcher) {
        this.rowCountMatcher = rowCountMatcher;

        return this;
    }

    public UserTableResultSetRowMatcherFactory andRow(int rowNumber) {
        return new UserTableResultSetRowMatcherFactory(this, rowNumber);
    }

    public void addRow(int rowNumber, UserTableMatcher rowMatcher) {
        rowMatchers.add(rowMatcher);
        // TODO Salvar a informação da linha para testar depois
    }

    public UserTableResultSetMatcher endResultSet() {
        UserTableResultSetMatcher product = new UserTableResultSetMatcher();
        
        product.setRowCountMatcher(this.rowCountMatcher);

        for (UserTableMatcher rowMatcher : rowMatchers)
            product.addRowMatcher(rowMatcher);

        return product ;
    }
}
