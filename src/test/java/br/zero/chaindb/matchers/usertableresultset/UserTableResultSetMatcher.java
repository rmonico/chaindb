package br.zero.chaindb.matchers.usertableresultset;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import br.zero.chaindb.matchers.usertable.UserTableMatcher;
import br.zero.chaindb.model.Node;
import br.zero.chaindb.model.UserTable;

public class UserTableResultSetMatcher extends BaseMatcher<ResultSet> {

    private static final String OK = "ok";
    private static final String IS_NULL = "is null";
    private static final String IS_NOT_A_INSTANCE_OF_RESULT_SET = "is not a instance of ResultSet";
    private static final String INCOMPATIBLE_STRUCTURE = "has incompatible structure";
    private static final String DIFFERENT_DATA = "has different data on line %d";
    private static final String ROW_COUNT_DOESNT_MATCH = "row count doesnt match (%d)";
    private final List<UserTableMatcher> rowMatcherList;
    private Matcher<Integer> rowCountMatcher;
    private final LinkedList<String> columnNames;
    private String matchResult;

    public UserTableResultSetMatcher() {
        rowMatcherList = new ArrayList<>();

        columnNames = new LinkedList<String>();

        columnNames.add("usertable_finaldata");
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("A UserTableResultSetMatcher ");

        if (rowMatcherList.isEmpty()) {
            description.appendText(" where should has no rows.");
        } else {
            description.appendList("where should have a row where ", " and a row with", ".", rowMatcherList);
        }
    }

    private String doResultSetStructureMatch(final ResultSet resultSet) throws SQLException {
        for (String columnName : columnNames) {
            try {
                resultSet.getObject(columnName);
            } catch (SQLException e) {
                return INCOMPATIBLE_STRUCTURE;
            }
        }

        return OK;
    }

    private String doContentMatch(final ResultSet resultSet) throws SQLException {
        int line = 0;

        while (resultSet.next()) {
            final Node node = new Node();

            node.setID(resultSet.getString("node_id"));
            node.setName(resultSet.getString("node_name"));

            // FIXME Extract a method here
            Calendar creation = GregorianCalendar.getInstance();
            creation.setTimeInMillis(resultSet.getInt("node_creation") * 1000);
            node.setCreation(creation);

            final UserTable userTable = new UserTable();

            userTable.setNode(node);
            userTable.setUserData(resultSet.getString("usertable_finaldata"));

            final UserTableMatcher rowMatcher = rowMatcherList.get(line);

            if (!rowMatcher.matches(userTable)) {
                return String.format(DIFFERENT_DATA, line);
            }

            line++;
        }

        if (rowCountMatcher != null && !rowCountMatcher.matches(line))
            return String.format(ROW_COUNT_DOESNT_MATCH, line);

        return OK;
    }

    public String doMatch(final Object object) throws SQLException {
        if (object == null)
            return IS_NULL;

        if (!(object instanceof ResultSet))
            return IS_NOT_A_INSTANCE_OF_RESULT_SET;

        final ResultSet resultSet = (ResultSet) object;

        try {
            String structureMatchResult = doResultSetStructureMatch(resultSet);

            if (!OK.equals(structureMatchResult))
                return structureMatchResult;

            return doContentMatch(resultSet);
        } finally {
            resultSet.close();
        }
    }

    @Override
    public boolean matches(final Object object) {
        try {
            matchResult = doMatch(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return OK.equals(matchResult);
    }

    @Override
    public void describeMismatch(final Object object,
            final Description description) {
        description.appendText("Object ");
        description.appendText(matchResult);

        if (INCOMPATIBLE_STRUCTURE.equals(matchResult))
            description.appendValueList(". It should have columns ['", "', '", "']", columnNames);
    }

    public void setRowCountMatcher(final Matcher<Integer> rowCountMatcher) {
        this.rowCountMatcher = rowCountMatcher;
    }

    public void addRowMatcher(final UserTableMatcher rowMatcher) {
        rowMatcherList.add(rowMatcher);
    }

}
