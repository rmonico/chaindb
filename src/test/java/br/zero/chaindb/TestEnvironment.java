package br.zero.chaindb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.sqlite.SQLiteConfig;

import br.zero.chaindb.dao.NodeDao;
import br.zero.chaindb.model.Node;

public class TestEnvironment implements Environment {

    private IDGenerator idGenerator;
    private Connection connection;
    private NodeDao nodeDao;
    private StatementFactory statementFactory;

    public TestEnvironment() {
        idGenerator = new IDGenerator();
    }

    @Override
    public void makeConnection() throws SQLException {
        SQLiteConfig configs = new SQLiteConfig();

        configs.enforceForeignKeys(true);

        connection = DriverManager.getConnection("jdbc:sqlite::memory:", configs.toProperties());
        statementFactory = new StatementFactory("src/test/resources/sqls/%s.sql");
    }

    @Override
    public void closeConnection() throws SQLException {
        statementFactory.invalidate();
        connection.close();
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public IDGenerator getIdGenerator() {
        return idGenerator;
    }

    @Override
    public void makeDAOs() throws SQLException {
        nodeDao = new NodeDao("23c2679cbf774ee18f6e048bdf60f7e1");
    }

    @Override
    public NodeDao getNodeDAO() {
        return nodeDao;
    }

    @Override
    public StatementFactory getStatementFactory() {
        return statementFactory;
    }

    @Override
    public Node getThisNode() {
        try {
            return getNodeDAO().getThisNode();
        } catch (SQLException e) {
            return null;
        }
    }

}
