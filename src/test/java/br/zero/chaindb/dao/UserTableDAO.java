package br.zero.chaindb.dao;

import static br.zero.chaindb.EnvironmentSingleTone.ENVIRONMENT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.zero.chaindb.IDGeneratorException;
import br.zero.chaindb.model.UserTable;

public class UserTableDAO {

    private void loadUserTableStatementIntoList(final PreparedStatement userTableStatement,
            final List<UserTable> list) throws SQLException {
        try (final ResultSet userTableResultSet = userTableStatement.executeQuery()) {
            while (userTableResultSet.next()) {
                final UserTable row = new UserTable();

                row.setID(userTableResultSet.getString("id"));
                row.setUserData(userTableResultSet.getString("finaldata"));

                list.add(row);
            }
        }
    }

    public List<UserTable> listActive() throws SQLException {
        final PreparedStatement preparedStatement = ENVIRONMENT.getStatementFactory().get("usertable/select_active_list");

        final ArrayList<UserTable> list = new ArrayList<>();

        loadUserTableStatementIntoList(preparedStatement, list);

        return list;
    }

    public void loadHistoryOf(UserTable row) throws SQLException {
        final PreparedStatement predecessorsStatement = ENVIRONMENT.getStatementFactory().get("generic/load_predecessors");

        predecessorsStatement.setString(1, row.getID().toString());

        loadUserTableStatementIntoList(predecessorsStatement, row.getPredecessors());

        final PreparedStatement sucessorsStatement = ENVIRONMENT.getStatementFactory().get("generic/load_sucessors");

        sucessorsStatement.setString(1, row.getID().toString());

        loadUserTableStatementIntoList(sucessorsStatement, row.getSucessors());
    }

    public UserTable create() throws SQLException {
        final UserTable newRow = new UserTable();

        newRow.setNode(ENVIRONMENT.getThisNode());

        return newRow;
    }

    public UserTable create(final UserTable previousVersion) {
        final UserTable newRow = new UserTable();

        return newRow;
    }

    public boolean persist(UserTable row) throws SQLException, IDGeneratorException {
        // TODO Controlar a transação
        if (row.getID() == null) {
            final CharSequence id = ENVIRONMENT.getIdGenerator().makeId(row);

            row.setID(id);

            // TODO Update sucessors/predecessors tables

            final PreparedStatement insertIntoObjectStatement = ENVIRONMENT.getStatementFactory().get("generic/insert_into");

            insertIntoObjectStatement.setString(1, row.getID().toString());
            insertIntoObjectStatement.setString(2, row.getNode().getID().toString());

            insertIntoObjectStatement.execute();

            final PreparedStatement insertIntoUserTableStatement = ENVIRONMENT.getStatementFactory().get("usertable/insert_into");

            insertIntoUserTableStatement.setString(1, row.getID().toString());
            insertIntoUserTableStatement.setString(2, row.getUserData());

            insertIntoUserTableStatement.execute();

            return true;
        } else {
            // TODO Update
            return false;
        }
    }

}
