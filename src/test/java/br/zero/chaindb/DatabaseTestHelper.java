package br.zero.chaindb;

import static br.zero.chaindb.EnvironmentSingleTone.ENVIRONMENT;
import static java.lang.String.format;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.zero.chaindb.model.Node;
import br.zero.chaindb.model.UserTable;

public class DatabaseTestHelper {

    public void makeDatabaseBasicStructure() throws SQLException {
        String minimalStructureSQL = ENVIRONMENT.getStatementFactory().getString("structure/minimal");

        try (final Statement statement = ENVIRONMENT.getConnection().createStatement()) {
            statement.executeUpdate(minimalStructureSQL);
        }
    }

    public CharSequence newRow(final CharSequence predecessorID, final CharSequence sucessorID, final String userData) throws IDGeneratorException, SQLException {
        final Node node = ENVIRONMENT.getThisNode();

        final UserTable row = new UserTable();

        row.setUserData(userData);
        row.setNode(node);

        final CharSequence id = ENVIRONMENT.getIdGenerator().makeId(row);

        row.setID(id);

        final Statement statement = ENVIRONMENT.getConnection().createStatement();

        statement.execute(format("insert into __object__ (id, source_node) values ('%s', '%s');", id, row.getNode().getID()));
        statement.execute(format("insert into usertable (id, finaldata) values ('%s', '%s');", id, row.getUserData()));

        if (predecessorID != null) {
            statement.execute(format("insert into __predecessor__ (id, predecessor) values ('%s', '%s');", id, predecessorID));
            statement.execute(format("insert into __sucessor__ (id, sucessor) values ('%s', '%s');", predecessorID, id));
        }

        if (sucessorID != null) {
            statement.execute(format("insert into __sucessor__ (id, sucessor) values ('%s', '%s');", id, sucessorID));
            statement.execute(format("insert into __predecessor__ (id, predecessor) values ('%s', '%s');", sucessorID, id));
        }

        return id;
    }

    public ResultSet loadUserTableAllRows() throws SQLException {
        PreparedStatement statement = ENVIRONMENT.getStatementFactory().get("usertable/select_all_rows");

        return statement.executeQuery();
    }
}
