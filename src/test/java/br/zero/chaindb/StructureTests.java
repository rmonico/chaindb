package br.zero.chaindb;

import static br.zero.chaindb.EnvironmentSingleTone.ENVIRONMENT;
import static br.zero.chaindb.matchers.usertable.UserTableMatcherFactory.whereUserTable;
import static br.zero.chaindb.matchers.usertableresultset.UserTableResultSetMatcherFactory.whereUserTableResultSet;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.zero.chaindb.dao.NodeDao;
import br.zero.chaindb.dao.UserTableDAO;
import br.zero.chaindb.matchers.usertable.UserTableMatcher;
import br.zero.chaindb.matchers.usertableresultset.UserTableResultSetMatcher;
import br.zero.chaindb.model.UserTable;

public class StructureTests {

    public NodeDao nodeDao;
    private DatabaseTestHelper helper;

    @BeforeClass
    public static void before_class() {
        ENVIRONMENT.injectEnvironment(new TestEnvironment());
    }

    @Before
    public void before_each_test() throws SQLException {
        ENVIRONMENT.makeConnection();

        helper = new DatabaseTestHelper();

        helper.makeDatabaseBasicStructure();

        ENVIRONMENT.makeDAOs();
    }

    @After
    public void after_each_test() throws SQLException {
        ENVIRONMENT.closeConnection();
    }

    @Test
    public void should_load_a_single_line() throws SQLException, IDGeneratorException, IOException {
        helper.newRow(null, null, "End user data");

        final UserTableDAO dao = new UserTableDAO();

        final List<UserTable> list = dao.listActive();

        assertThat("Loaded", list, hasSize(1));

        final UserTable row = list.get(0);

        assertThat("data", row, whereUserTable().userData(is("End user data")).predecessors(empty()).sucessors(empty()).ok());
    }

    @Test
    public void should_load_a_line_with_history() throws IDGeneratorException, SQLException {
        final CharSequence firstId = helper.newRow(null, null, "First version of data");
        final CharSequence secondId = helper.newRow(firstId, null, "Second version of data");
        helper.newRow(secondId, null, "Third, last and active version of data");

        final UserTableDAO dao = new UserTableDAO();

        final List<UserTable> list = dao.listActive();

        assertThat("Loaded", list, hasSize(1));

        final UserTable activeVersion = list.get(0);
        dao.loadHistoryOf(activeVersion);
        assertThat("final_version", activeVersion, whereUserTable().userData(is("Third, last and active version of data")).predecessors(hasSize(1)).sucessors(empty()).ok());

        final UserTable previousVersion = activeVersion.getPredecessors().get(0);
        dao.loadHistoryOf(previousVersion);
        assertThat("previous_version", previousVersion, whereUserTable().userData(is("Second version of data")).predecessors(hasSize(1)).sucessors(hasSize(1)).ok());

        final UserTable initialVersion = previousVersion.getPredecessors().get(0);
        dao.loadHistoryOf(initialVersion);
        assertThat("Initial version", initialVersion, whereUserTable().userData(is("First version of data")).predecessors(empty()).sucessors(hasSize(1)).ok());
    }

    @Test
    public void should_save_a_line_with_no_history() throws SQLException, IDGeneratorException {
        final UserTableDAO dao = new UserTableDAO();

        final UserTable newRow = dao.create();

        newRow.setUserData("Row data");

        dao.persist(newRow);

        final ResultSet actual = helper.loadUserTableAllRows();

        assertThat("Persisted data", actual, whereUserTableResultSet().rowCount(is(1)).andRow(0).data(is("Row data")).sucessorList(is(empty())).predecessorList(is(empty())).endRow().endResultSet());
    }

    @Test
    public void should_save_a_line_with_history() throws SQLException, IDGeneratorException {
        final UserTableDAO dao = new UserTableDAO();

        final UserTable firstVersion = dao.create();

        firstVersion.setUserData("First version of data");

        final UserTable secondVersion = dao.create(firstVersion);

        secondVersion.setUserData("Second version of data");

        final UserTable thirdVersion = dao.create(secondVersion);

        thirdVersion.setUserData("Third and, for now, final version of data");
        
        dao.persist(firstVersion);

        final UserTableResultSetMatcher resultSetMatcher = new UserTableResultSetMatcher();

        resultSetMatcher.addRowMatcher(new UserTableMatcher(is("Row data"), empty(), empty()));

        final ResultSet actual = helper.loadUserTableAllRows();

        assertThat("Persisted data", actual, resultSetMatcher);

    }

    // Depois fazer testes com estrutura envolvendo chave estrangeira
}
