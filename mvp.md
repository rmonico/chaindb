# MVP (Minimum Viable Product)

## Requisitos Funcionais (por ordem de relevância)
- Armazenar os dados em um esquema de banco de dados relacional [**ok**]
- Fazer lançamentos em um ponto e recebe-los em outro sem intervenção
- Não ficar bloqueado quando surgirem versões concorrentes da mesma informação
- Merge de dados concorrentes
- Quando uma linha for atualizada, as linhas de outras tabelas que fizerem referência à linha que foi atualizada precisam ser atualizadas também [*definir*, pensar melhor se deve ser responsabilidade do componente fazer isso]
    - Responsabilidade do DAO?

## Versões posteriores:
- Possibilidade de incorporar novos pontos ao cluster [*opcional*]
- Possibilidade de excluir pontos do cluster [*opcional*]
- Limpeza de dados antigos [*opcional*]
    - Possíveis conflitos para sincronizar com pontos com alterações pendentes que não sofreram limpeza
- Suporte à diferentes bancos de dados? [*definir*]
- Implementação de pontos em diferentes linguagens? [*definir*]